YREC Markup Language (YML)
==========================

This is a human-readable executable format based on YAML for templating,
generating, and executing YREC runs in Python.

Everything is templated using Jinja2, so Jinja2 expressions should work.

The included template.yml file is an example template YML file. It is divided
into three parts: Runs, nml1, nml2.

New runs can be defined by adding a human-readable item to the Runs section.

Variables defined in the Runs sections are not added to the Namelists, but they
can be referenced from elsewhere in the YML file.

## Example

    git clone https://bitbucket.org/rrlyrae/yrecml/src/master/

Simply edit `YRECRootDirectory`, `YRECNamelistDirectory`, and ensure that you
have the correct input files for the FFIRST (currently I just copy over gs98
to gs98_renamed and express the mass with a decimal, so `p040gs98z01757.ahbl` to
`p0.40gs98z01757.ahbl`). Then spin up a Python interactive terminal and run:

    import yml
    namelist_locations = yml.parse('template.nml')

If you are running the code as well, try:

    run_parallel(namelist_locations, pool_size = 20, command = "../model5.0")

Replacing `command` with the appropriate pointer to YREC from the current
directory (a symlink is probably fine, or an absolute path).

## Runs

The keywords underneath `Runs` are:

- `YRECRootDirectory`: absolute path of YREC root
- `YRECNamelistDirectory`: absolute path of the namelist output directory
- `folder_name`: relative path (rel. to YREC root) of the YREC data output
directory
- `kindrns`: the list of kindrns for this particular run.

### Generating grids of runs:

The parser will detect any lists in any given `Run`, and iterate over each one
in turn. For example, if template.yml has:

    Mass:
       - "0.15"
       - "0.20"
    LROT:
       - ".TRUE."
       - ".FALSE."

The code will generate a rotating and a non-rotating run for each of the 0.15
and 0.20 solar mass models.

### Templating:

Adding double curly brackets and a variable name inside will prompt Jinja2 to
fill in that value from a keyword under a defined `Run`.

Because of how YAML and Jinja2 interact with strings, most Jinja2 expressions
need to be encapsulated in quotes `"`, rather than `'`.

Sometimes YREC cares too about how strings are handled, so in certain cases,
such as templating with files, you'll need to put a single quote in a double
quote. A quote like this `""` is interpreted in YAML as an empty string, and
will error on YREC's end when it tries to find a string and finds NULL.

### Contact me!

Contact Lyra with Q's, suggestions, etc.
