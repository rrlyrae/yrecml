import yaml
import jinja2 as jj2
import copy
import itertools
import os
import multiprocessing
from multiprocessing.pool import ThreadPool
import subprocess
import time

def call_proc(cmd, template_nml_full_path, index_of_cmd = 0, total_length = 0):
	"""Logs runs to stdout and stderr."""
	print ("Executing command ", index_of_cmd, "/", total_length, ":", cmd)
	start_time = time.time()
	with open(template_nml_full_path+".stdout", "w") as stdout:
		with open(template_nml_full_path+".stderr", "w") as stderr:
			p = subprocess.Popen(cmd, shell = True, stdout=stdout, stderr=stderr)
			out, err = p.communicate()
	print ("Completed command ", index_of_cmd, "/", total_length, "in", time.time() - start_time, "s.")
	return (out, err)

def parse(template_filename = "template.yml"):
	"""Resolves the template YML file, returning a list of namelists."""
	namelists_to_run = []
	with open(template_filename) as input_file:
		yrecnml = yaml.load(input_file, Loader=yaml.FullLoader)
		if "Runs" in yrecnml:
			for run in yrecnml["Runs"]:
				run_name = run["Name"]
				print ("Parsing "+repr(run_name))
				nml1_dict = copy.deepcopy(yrecnml["nml1"])
				nml2_dict = copy.deepcopy(yrecnml["nml2"])
				### First, define all the variables to template on:
				### These variables do not get appended to the namelists, but they
				### replace values in the template for the namelists.

				dict_iterate = {} ### These are iteration targets for the runs.
				dict_replace = {} ### These are replacement targets.
				list_iterate = [] ### We populate this with appropriate runs.
				### We move keys from iterate to replace.

				for run_tag in run:
					if ((run_tag != "Extras_NML1") and \
						(run_tag != "Extras_NML2") and \
						(run_tag != "kindrns")):
						if isinstance(run[run_tag], list) :
							dict_iterate[run_tag] = copy.deepcopy(run[run_tag])
						else:
							dict_replace[run_tag] = copy.deepcopy(run[run_tag])

				### Now that we have assembled our iterators in a dictionary,
				### We need to explode a dict with index parameters to
				### a list of individual runs.

				keys_iterate = list(dict_iterate.keys())
				vals_iterate = list(dict_iterate.values())
				for item in itertools.product(*vals_iterate):
					temp_run_iter = copy.deepcopy(dict_replace)
					temp_run_iter.update(dict(zip(keys_iterate, item)))
					list_iterate.append(temp_run_iter)
				if len(list_iterate) < 1:
					temp_run_iter = copy.deepcopy(dict_replace)
					list_iterate.append(temp_run_iter)

				assert "kindrns" in run, "Must have a kindrn defined."

				### Analyze kindrns:
				kindrns = copy.deepcopy(run["kindrns"])
				num_kindrns = len(kindrns)
				### For the kindrns, for the keys add '(idx)'
				nml1_dict['NUMRUN'] = str(num_kindrns)
				for kindrn_idx, kindrn in enumerate(kindrns):
					for key, val in kindrn.items():
						nml1_dict[key+'('+str(kindrn_idx+1)+')'] = val

				### Allow some "Extras" just in case
				if "Extras_NML1" in run:
					nml1_dict.update(copy.deepcopy(run["Extras_NML1"]))
				if "Extras_NML2" in run:
					nml2_dict.update(copy.deepcopy(run["Extras_NML2"]))

				### Now, we have the base namelists and we will let the jj2
				### do the appropriate templating as we iterate over list_iterate:
				for run_idx, run_iter in enumerate(list_iterate):
					print('Generating NAMELIST', run_idx+1, "/", len(list_iterate))
					print('Variable template', run_iter)
					### Generate the namelist itself:
					nml1_text = '$CONTROL\n'
					nml2_text = '$PHYSICS\n'
					for nml1_entry in nml1_dict:
						nml1_text += nml1_entry + ' = ' + str(nml1_dict[nml1_entry]) +"\n"
					for nml2_entry in nml2_dict:
						nml2_text += nml2_entry + ' = ' + str(nml2_dict[nml2_entry]) +"\n"
					nml1_text += '\n$END'
					nml2_text += '\n$END'
					### Resolve the texts inside the actual iteration strings:
					for key in run_iter:
						run_iter[key] = jj2.Template(str(run_iter[key])).render(**run_iter)
					jj2_nml1 = jj2.Template(nml1_text).render(**run_iter)
					jj2_nml2 = jj2.Template(nml2_text).render(**run_iter)

					### Output:
					if not os.path.exists(run_iter["YRECNamelistDirectory"].strip()):
						os.makedirs(run_iter["YRECNamelistDirectory"].strip())
					if not os.path.exists(run_iter["YRECRootDirectory"].strip()+run_iter["folder_name"].strip()):
						os.makedirs(run_iter["YRECRootDirectory"].strip()+run_iter["folder_name"].strip())

					with open(run_iter["YRECNamelistDirectory"]+run_iter["fname"]+".nml1", 'w') as output_file:
						output_file.write(jj2_nml1)
					with open(run_iter["YRECNamelistDirectory"]+run_iter["fname"]+".nml2", 'w') as output_file:
						output_file.write(jj2_nml2)

					### Append this namelist to a list to be returned by this function.
					namelists_to_run.append(run_iter["YRECNamelistDirectory"]+run_iter["fname"])
	return namelists_to_run

def run_parallel(namelists, pool_size = 20, command = "../model5.0"):
	"""Call this with a list of namelists, e.g. from `parse`."""
	start_time = time.time()
	pool = ThreadPool(pool_size)
	for namelist_idx, namelist in enumerate(namelists):
		cmd = command+(" %s %s" % (namelist+".nml1", namelist+".nml2"))
		print ("Dispatching to pool: ", namelist_idx+1, "/", len(namelists))
		pool.apply_async(call_proc, (cmd, namelist, namelist_idx+1, len(namelists)))
	pool.close()
	pool.join()
	print (len(namelists), "tasks completed in", time.time() - start_time, "s.")
	return True
